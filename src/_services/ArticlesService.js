import http from "../http-common";

class ArticlesService {
  getAll() {
    return http.get("/articles");
  }
  findByCategory(category) {
    return http.get(`/articles?filter=${category}`);
  }
}

export default new ArticlesService();
